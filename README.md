# Avisota 2

Avisota is a high definition newsletter and mailing system for the Contao CMS <www.contao.org>.

## Events

### avisota-recipient-subscribe (SubscribeEvent)

Triggered if a recipient starts the subscription process (double-opt-in).

### avisota-recipient-confirm-subscription (ConfirmSubscriptionEvent)

Triggered if a recipient confirms his subscription.

### avisota-recipient-unsubscribe (UnsubscribeEvent)

Triggered if a recipient cancels his subscription.

### avisota-recipient-remove (RecipientEvent)

Triggered if a recipient gets removed, because he has no more subscriptions.
