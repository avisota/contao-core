<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['0']                       = 'Przypisz kolumny';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['1']                       = 'Proszę przypisać kolumny do wewnętrznych pól.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentColumn']['0']                 = 'Kolumna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['0']                  = 'Przypisanie';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['0']                                = 'Plik CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['1']                                = 'Proszę wybrać plik CSV.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFile_legend']                                 = 'Plik CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csv_file']['0']                                  = 'Plik CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['0']                                    = 'Usuń źródło odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['1']                                    = 'Usuń źródło odbiorców ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['details_legend']                                 = 'Szczegółowe ustawienia';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['0']                                   = 'Wyłączone';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['1']                                   = 'Tymczasowo wyłącza źródło odbiorców dla transportu.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['0']                                      = 'Zmniejsz priorytet';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['1']                                      = 'Zmniejsz priorytet źródła odbiorców ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['0']                                     = 'Losowy generator (tylko do testów!)';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['1']                                     = 'Losowo wygeneruj odbiorców z predefiniowanego zestawu imion, nazwisk i domen. To źródło odbiorców jest użyteczne dla testów. Wszystkie adresy e-mail są wygenerowane. Nie powinny one istnieć, ale nie ma na to gwarancji! To źródło odbiorców powinno być tylko użyte w połączeniu z testowym transportem lub tryb developera.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['0']                             = 'Maksymalna liczba';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['1']                             = 'Proszę wprowadzić maksymalną liczbę wygenerowanych odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['0']                             = 'Minimalna liczba';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['1']                             = 'Proszę wprowadzić minimalną liczbę wygenerowanych odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy_legend']                                   = 'Ustawienia generatora';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['duplicated_column']                              = 'Kolumny i wiersze nie mogą być użyte dwa razy!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['0']                                      = 'Edytuj źródło odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['1']                                      = 'Edytuj źródło odbiorców ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['expert_legend']                                  = 'Zaawansowane ustawienia';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['0']                                    = 'Włącz filtry';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['1']                                    = 'Włącz filtry w tym źródle odbiorców. Dostępne filtry zależą od konkretnego modułu.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter_legend']                                  = 'Ustawienia filtrów';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated']['0']                                = 'Zintegrowani odbiorcy Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleListSelection']['0']        = 'Pozwól na pojedynczy wybór list mailingowych';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleListSelection']['1']        = 'Pozwól na pojedynczy wybór list mailingowych z tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleSelection']['0']            = 'Pozwól na pojedynczy wybór odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleSelection']['1']            = 'Pozwól na pojedynczy wybór odbiorców z tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['0']                              = 'Wybierz odbiorców&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['1']                              = 'Proszę wybrać jak odbiorcy są wybierani.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllMailingLists']                    = 'przez wszystkie listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllRecipients']                      = 'przez wszystkich odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByMailingLists']                       = 'przez wybrane listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByRecipients']                         = 'przez odbiorców z wybranych list mailingowych';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['0']                         = 'Pobierz szczegóły z&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['1']                         = 'Proszę wybrać skąd powinny być pobierane szczegóły.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['0']                 = 'Filtr kolumny';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['1']                 = 'Filtruj odbiorców po kolumnach.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsComparator']['0']       = 'Porównanie';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsField']['0']            = 'Kolumna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['0']         = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['1']         = 'Użyj wartości jako natywnego SQL (&rarr; wartość nie zostanie dostosowana).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsValue']['0']            = 'Wartość';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['0']          = 'Listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['1']          = 'Proszę wybrać listy mailingowe. Dostępni są tylko odbiorcy, którzy subskrybowali wybrane listy mailingowe.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['0'] = 'Strona zarządzania subksrypcją';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['1'] = 'Proszę wybrać stronę zarządzania subskrypcją.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_by_mailing_list']['0']                = 'Zintegrowani odbiorcy Avisota wybrani z list mailingowych';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_details']                             = 'Zintegrowani odbiorcy Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_legend']                              = 'Zintegrowani odbiorcy Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_member_details']                      = 'Zintegrowani odbiorcy Avisota i użytkownicy Contao.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['0']                              = 'Listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['1']                              = 'Proszę wybrać listy mailingowe.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member']['0']                                    = 'Użytkownicy Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['0']           = 'Pozwól na pojedynczy wybór grup';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['1']           = 'Pozwól na pojedynczy wybór grup z tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['0']     = 'Pozwól na pojedynczy wybór list mailingowych.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['1']     = 'Pozwól na pojedynczy wybór list mailingowych z tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['0']                = 'Pozwól na pojedynczy wybór użytkowników';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['1']                = 'Pozwól na pojedynczy wybór użytkowników z tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['0']                                  = 'Wybierz użytkowników&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['1']                                  = 'Proszę wybrać jak wybierani są użytkownicy.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllGroups']                              = 'przez wszystkie grupy';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMailingLists']                        = 'przez wszystkie listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMembers']                             = 'przez wszystkich użytkowników';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroupMembers']                           = 'przez użytkownik z wybranych grup';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroups']                                 = 'przez wybrane grupy';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingListMembers']                     = 'przez użytkowników z wybranych list mailingowych';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingLists']                           = 'przez wybrane listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['0']                     = 'Filtr kolumny';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['1']                     = 'Filtruj odbiorców po kolumnach.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsComparator']['0']           = 'Porównanie';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsField']['0']                = 'Kolumna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['0']             = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['1']             = 'Użyj wartości jako natywnego SQL (&rarr; wartość nie zostanie dostosowana).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsValue']['0']                = 'Wartość';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['0']                              = 'Grupy';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['1']                              = 'Proszę wybrać grupy.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['0']                        = 'Listy mailingowe';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['1']                        = 'Proszę wybrać listy mailingowe.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_details']                                 = 'Użytkownicy Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_legend']                                  = 'Użytkownicy Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['missing_email_column']                           = 'Musisz przypisać adres e-mail do jednej kolumny!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['0']                                       = 'Nowe źródło odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['1']                                       = 'Dodaj nowe źródło odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['0']                                      = 'Szczegóły źródła odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['1']                                      = 'Pokaż szczegóły źródła odbiorców ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['source_legend']                                  = 'Źródło odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['0']                                     = 'Nazwa';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['1']                                     = 'Proszę wprowadzić nazwę dla tego źródła odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['0']                                    = 'Przełącz wyłączony status';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['1']                                    = 'Przełącz wyłączony status źródła odbiorców ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['0']                                      = 'Moduł źródła odbiorców';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['1']                                      = 'Proszę wybrać moduł źródła danych odbiorców.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['0']                                        = 'Zwiększ priorytet';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['1']                                        = 'Zwiększ priorytet źródła odbiorców ID %s';
