<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['avisota_outbox']['action_execute']        = 'Wykonaj kolejkę';
$GLOBALS['TL_LANG']['avisota_outbox']['col_duration']          = 'czas trwania';
$GLOBALS['TL_LANG']['avisota_outbox']['col_failed']            = 'niepowodzenie';
$GLOBALS['TL_LANG']['avisota_outbox']['col_length']            = 'Długość';
$GLOBALS['TL_LANG']['avisota_outbox']['col_name']              = 'Nazwa kolejki';
$GLOBALS['TL_LANG']['avisota_outbox']['col_open']              = 'otwarty';
$GLOBALS['TL_LANG']['avisota_outbox']['col_success']           = 'sukces';
$GLOBALS['TL_LANG']['avisota_outbox']['col_timeout']           = 'czas minął';
$GLOBALS['TL_LANG']['avisota_outbox']['execute']               = 'Wykonaj kolejkę %s';
$GLOBALS['TL_LANG']['avisota_outbox']['headline']              = 'Skrzynka nadawcza';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_error']        = 'Wystąpił błąd, proces wstrzymany!';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_finish']       = 'Wysyłanie zakończone, teraz możesz zamknąć okno.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_initializing'] = 'Inicjalizacja, proszę czekać i nie zamykać okna.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_pause']        = 'Czekanie na następny cykl, proszę nie zamykać okna.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_running']      = 'Wysyłanie wiadomości, proszę czekać i nie zamykać okna.';
