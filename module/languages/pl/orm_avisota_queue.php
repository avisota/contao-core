<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['0']                    = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['1']                    = 'Alias kolejki jest unikalnym odwołaniem do kolejki, które może być użyte zamiast ID.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['0']       = 'Pozwól na ręcznie wysyłanie';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['1']       = 'Pozwól użytkownikom na ręczne wykonanie kolejki i wysłanie jej zawartości.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['config_legend']                 = 'Ustawienia kolejki';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['0']               = 'Przerwa pomiędzy cyklami';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['1']               = 'Proszę wprowadzić czas przerwy w sekundach pomiędzy cyklami.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['0']                   = 'Usuń kolejkę';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['1']                   = 'Usuń kolejkę ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['0']                     = 'Edytuj kolejkę';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['1']                     = 'Edytuj kolejkę ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['0']             = 'Licznik wysyłania';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['1']             = 'Proszę wprowadzić maksymalną liczbę e-maili dla każdego cyklu.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['0']              = 'Czas wysyłania';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['1']              = 'Proszę wprowadzić maksymalny czas w sekundach dla każdego cyklu.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['0']                      = 'Nowa kolejka';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['1']                      = 'Utwórz nową kolejkę';
$GLOBALS['TL_LANG']['orm_avisota_queue']['queue_legend']                  = 'Kolejka';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['0']         = 'Planowane wysyłanie';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['1']         = 'Użyj algorytmu planowanego wysyłania dla automatycznego wykonywania.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['send_legend']                   = 'Wysyłanie';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['0']              = 'Wykres czasu wysyłania';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['1']              = 'Wykres czasu, który określa czasy wykonania.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['0']                     = 'Szczegóły kolejki';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['1']                     = 'Pokaż szczegóły kolejki ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabase']                = 'Prosta kolejka oparte na bazie danych';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['0'] = 'Nazwa tabeli';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['1'] = 'Proszę wprowadzić nazwę tabeli dla kolejki. Tabela <strong>nie powinna</strong> zaczynać się z <em>tl_</em> lub <em>orm_</em>!';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['0']                    = 'Tytuł';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['1']                    = 'Proszę wprowadzić tytuł kolejki.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['0']                = 'Moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['1']                = 'Proszę wybrać moduł transportu.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport_legend']              = 'Ustawienia transportu';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['0']                     = 'Typ';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['1']                     = 'Proszę wybrać typ kolejki.';
