<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['0'] = 'Domyślny moduł transportu';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['1'] = 'Proszę wybrać domyślny moduł transportu.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['0']   = 'Adres e-mail developera';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['1']   = 'Proszę wprowadzić adres e-mail, który będzie użyta dla każdego e-maila w trybie developera.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['0']    = 'Tryb developera';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['1']    = 'Włącz tryb developera.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['developer_legend']               = 'Developer';
$GLOBALS['TL_LANG']['tl_avisota_settings']['edit']                           = 'Ustawienia systemu Avisota';
$GLOBALS['TL_LANG']['tl_avisota_settings']['transport_legend']               = 'Transport';
