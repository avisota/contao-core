<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['0']                                     = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['1']                                     = 'Alias listy mailingowej jest unikalnym odwołaniem do listy mailingowej, które może być użyte zamiast ID.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['0']                                      = 'Duplikuj listę mailingową';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['1']                                      = 'Duplikuj listę mailingową ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['0']                                    = 'Usuń listę mailingową';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['1']                                    = 'Usuń listę mailingową ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['0']                                      = 'Edytuj listę mailingową';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['1']                                      = 'Edytuj listę mailingową ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['expert_legend']                                  = 'Zaawansowane ustawienia';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['0'] = 'Strona do zarządzania subskrypcją tej listy';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['1'] = 'Proszę wybrać stronę, gdzie odbiorcy mogą zarządzać ich subskrypcją tej listy mailingowej.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_members']                                  = '%1$d użytkowników (<span title="%2$d aktywnych użytkowników">%2$d</span> / <span title="%3$d nieaktywnych użytkowników">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_recipients']                               = '%1$d odbiorców (<span title="%2$d aktywnych odbiorców">%2$d</span> / <span title="%3$d nieaktywnych odbiorców">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['list_legend']                                    = 'Lista mailingowa';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['0']                                       = 'Nowa lista mailingowa';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['1']                                       = 'Dodaj nową listę mailingową';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['0']                                      = 'Szczegóły listy mailingowej';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['1']                                      = 'Pokaż szczegóły listy mailingowej ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['0']                                     = 'Nazwa';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['1']                                     = 'Proszę wprowadzić nazwę listy mailingowej.';
