<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['MOD']['avisota']['0']                   = 'Newsletter Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_config:newsletter']['0'] = 'Konfiguracja wiadomości';
$GLOBALS['TL_LANG']['MOD']['avisota_config:recipient']['0']  = 'Konfiguracja odbiorcy';
$GLOBALS['TL_LANG']['MOD']['avisota_config:transport']['0']  = 'Konfiguracja transportu';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['0']      = 'Listy mailingowe';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['1']      = 'Zarządzaj listami mailingowymi, które mogą być subskrybowane.';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter']['0']        = 'Wiadomości';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['0']  = 'Szkice wiadomości';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['1']  = 'Zarządzaj szkicami newslettera, aby tworzyć z nich nowe newslettery.';
$GLOBALS['TL_LANG']['MOD']['avisota_outbox']['0']            = 'Skrzynka nadawcza';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['0']             = 'Kolejki';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['1']             = 'Zarządzaj kolejkami transportów.';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['0']  = 'Źródła odbiorców';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['1']  = 'Zarządzaj źródłami odbiorców.';
$GLOBALS['TL_LANG']['MOD']['avisota_recipients']['0']        = 'Odbiorcy';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['0']        = 'Powitanie';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['1']        = 'Zarządzaj powitaniami.';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['0']          = 'Ustawienia systemu Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['1']          = 'Zarządzaj podstawowymi ustawieniami systemu Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['0']             = 'Motyw';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['1']             = 'Zarządzaj ustawieniami motywów newslettera, włączając w to pliki szablonów, style i szablony.';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['0']         = 'Transporty';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['1']         = 'Zarządzaj modułami transportów.';
