<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['1']              = 'Alias modułu transportu jest unikalnym odwołaniem do modułu transportu, które może być użyte zamiast ID.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['contact_legend']          = 'Ustawienia "od", nadawcy i "odpowiedzi"';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['0']             = 'Usuń moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['1']             = 'Usuń moduł transportu ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['0']               = 'Edytuj moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['1']               = 'Edytuj moduł transportu ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['0']        = 'Adres "od"';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['1']        = 'Proszę wprowadzić adres "od".';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['0']           = 'Nazwa "od"';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['1']           = 'Proszę wprowadzić nazwę "od".';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['0']                = 'Nowy moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['1']                = 'Utwórz nowy moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['0']     = 'Adres odpowiedzi';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['1']     = 'Proszę wprowadzić adres odpowiedzi.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['0']        = 'Nazwa "odpowiedzi"';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['1']        = 'Proszę wprowadzić nazwę "odpowiedzi".';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['0']      = 'Adres nadawcy';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['1']      = 'Proszę wprowadzić adres nadawcy.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['0']         = 'Nazwa nadawcy';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['1']         = 'Proszę wprowadzić nazwę nadawcy.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service']                 = 'Własna usługa';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['0']        = 'Nazwa usługi';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['1']        = 'Proszę wprowadzić nazwę usługi.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service_legend']          = 'Usługa';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['0']         = 'Ustaw adres odpowiedzi';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['1']         = 'Ustaw adres odpowiedzi na ten newsletter.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['0']          = 'Ustaw adres nadawcy';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['1']          = 'Ustaw nadawcę tego newslettera.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['0']               = 'Szczegóły modułu transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['1']               = 'Pokaż szczegóły modułu transportu ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift']                   = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['0']       = 'Szyfrowanie SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['1']       = 'Tutaj możesz wybrać metodę szyfrowania (SSL lub TLS).';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['0']      = 'Nazwa hosta SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['1']      = 'Proszę wprowadzić nazwę hosta serwera SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOff']            = 'Wyślij e-maile przez PHP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOn']             = 'Wyślij e-maile przez SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['0']      = 'Hasło SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['1']      = 'Tutaj możesz wprowadzić hasło SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['0']      = 'Numer portu SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['1']      = 'Proszę wprowadzić numer portu serwera SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpSystemSettings'] = 'Użyj ustawień systemu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['0']      = 'Użytkownikw SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['1']      = 'Tutaj możesz wprowadzić nazwę użytkownika SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['0']       = 'Wyślij e-maile przez SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['1']       = 'Użyj serwera SMTP zamiast funkcji mail() PHP do wysłana e-maili.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift_legend']            = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['0']              = 'Tytuł';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['1']              = 'Proszę wprowadzić tytuł modułu transportu.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['transport_legend']        = 'Moduł transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['0']               = 'Typ transportu';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['1']               = 'Proszę wybrać typ transportu.';
