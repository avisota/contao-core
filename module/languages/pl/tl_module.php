<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/pl/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['tl_module']['avisota_categories']['0']                    = 'Kategorie';
$GLOBALS['TL_LANG']['tl_module']['avisota_cleanup_legend']                     = 'Czyszczenie';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['0']                   = 'Strona docelowa formularza (nie strona potwierdzenia!)';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['1']                   = 'Proszę wybrać strona, do której będzie wysłany formularz.';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_template']['0']                 = 'Szablon listy';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['0']                         = 'Listy mailingowe';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['1']                         = 'Proszę wybrać listy mailingowe, które będą pokazane lub do których zostanie zapisany odbiorca.';
$GLOBALS['TL_LANG']['tl_module']['avisota_mail_legend']                        = 'Ustawienia e-mail';
$GLOBALS['TL_LANG']['tl_module']['avisota_notification_legend']                = 'Pamięć';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_template']['0']               = 'Szablon listy';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['0']              = 'Dane osobiste';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['1']              = 'Proszę wybrać dodatkowe dane osobiste, o które zostanie poproszony odbiorca.';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['0']                    = 'Pokaż listy mailingowe';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['1']                    = 'Pokaż listy mailingowe i pozwól odbiorcy na wybranie list, do których chce się zapisać.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['0']   = 'Strona potwierdzenia subskrypcji';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['1']   = 'Proszę wybrać stronę, która będzie pokazana przy pomyślnej subskrypcji.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscription_legend']                = 'Subskrypcja';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['0']            = 'Szablon formularza';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['1']            = 'Proszę wybrać szablon formularza.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['0']         = 'Szablon formularza';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['1']         = 'Proszę wybrać szablon formularza.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_unsubscribe']['0']          = 'Szablon formularza';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['0'] = 'Strona anulowania subskrypcji';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['1'] = 'Proszę wybrać stronę, która będzie pokazana przy pomyślnym anulowaniu subskrypcji.';
