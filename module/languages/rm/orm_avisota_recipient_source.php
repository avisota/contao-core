<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:12+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['0']                       = 'Attribuir colonnas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['1']                       = 'Attribuescha las colonnas als champs interns';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentColumn']['0']                 = 'Colonnas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['0']                  = 'Champ';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['1']                  = 'Endatescha il num da champ assegnà. P.ex <code>prenum</code> u <code>num</code>. Almain ina colonna <code>email</code> sto exister!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiter']['0']                          = 'Limitader';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiter']['1']                          = 'Tscherna il limitader da CSV.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['comma']                     = 'Comma';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['linebreak']                 = 'Nova lingia';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['separator']                 = 'Seperatur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['space']                     = 'Distanza';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['tabulator']                 = 'Tabulatur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosure']['0']                          = 'Brancar';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosure']['1']                          = 'Tscherna il tip da brancar da CSV.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosures']['double']                    = 'Virgulettas dublas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosures']['single']                    = 'Virgulettas simplas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['0']                                = 'Datoteca da CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['1']                                = 'Tscherna la datoteca da CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFile_legend']                                 = 'Datoteca da CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csv_file']['0']                                  = 'Datoteca da CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['0']                                    = 'Stizzar la funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['1']                                    = 'Stizzar la funtauna da destinaturs cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['details_legend']                                 = 'Configuraziun dals detagls';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['0']                                   = 'Deactivà';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['1']                                   = 'Deactivar temporarmain questa funtauna da destinaturs per il transport.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['0']                                      = 'Sbassar la prioritad';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['1']                                      = 'Sbassar la prioritad da la funtauna da destinaturs cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['0']                                     = 'Generatur da casualitad (be per testar!)';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['1']                                     = 'Generar caualmain destinatur da in set predefinà da prenums, nums e domains. Questa funtauna da destinaturs è practica per testar. Fa stim che tut las adressas dad e-mail èn be generadas. Ellas na duessan betg esser existentas, ma quai na pudain nus betg garantir! Questa funtauna da destinaturs duess be vegnir utilisada cun in dummy-transport u en il modus da sviluppader.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['0']                             = 'Dumber maximal';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['1']                             = 'Endatescha il dumber maximal da destinaturs generads';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['0']                             = 'Dumber minimal';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['1']                             = 'Endatescha il dumber minimal da destinaturs generads';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy_legend']                                   = 'Configuraziun dal generatur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['duplicated_column']                              = 'Colonnas e champs na pon betg vegnir utilisads duas giadas!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['0']                                      = 'Modifitgar la funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['1']                                      = 'Modifitgar la funtauna da destinaturs cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['expert_legend']                                  = 'Configuraziun avanzada';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['0']                                    = 'Activar filters';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['1']                                    = 'Activar filters sin questa funtauna da destinaturs. Ils filters disponibels dependan dal modul specific.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filterByMailingLists']['0']                      = 'Filtrar tenor ils abunaments';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filterByMailingLists']['1']                      = 'Activar la pussaivladad da filtrar tenor ils abunaments da glistas dad e-mail.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter_legend']                                  = 'Configuraziun dal filter';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated']['0']                                = 'Destinaturs integrads en Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleListSelection']['0']        = 'Lubir da tscherner be ina glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleListSelection']['1']        = 'Lubir a l\'autur da tscherner singuals glistas da mail da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleSelection']['0']            = 'Lubir da tscherner be in destinatur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleSelection']['1']            = 'Lubir a l\'autur da be tscherner in destinatur da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['0']                              = 'Tscherner destinatur&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['1']                              = 'Tscherna sco ch\'ils destinaturs vegnan tschernids.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllMailingLists']                    = 'tenor tut las glistas dad e-mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllRecipients']                      = 'tenor tut ils destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByMailingLists']                       = 'tenor las glistas dad e-mail selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByRecipients']                         = 'tenor destinaturs da las glistas dad e-mail selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['0']                         = 'Retschaiver ils detagls da&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['1']                         = 'Tscherne danunder ch\'ils detagls duain vegnir retschavids.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['0']                 = 'Filter da colonnas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['1']                 = 'Filtrar ils destinaturs tenor colonnas.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsComparator']['0']       = 'Cumparegliader';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsField']['0']            = 'Colonna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['0']         = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['1']         = 'Utilisar SQL navig (&rarr; la valur navegn betg controllà!).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsValue']['0']            = 'Valur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['0']          = 'Glistas da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['1']          = 'Tscherna las glistas da mail. Be destinaturs che abuneschan las glistas da mail tschernidas èn disponibels.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['0'] = 'Pagina d\'administraziun dils abunaments';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['1'] = 'Tscherna la pagina per l\'administraziun dals abunaments.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_by_mailing_list']['0']                = 'Destinaturs integrads en Avisota e selecziunads da la glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_details']                             = 'Destinaturs integrads en Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_legend']                              = 'Destinaturs integrads en Avisota';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_member_details']                      = 'Destinaturs integrads en Avisota e commembers da Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['list']['0']                                      = 'Mussar ils destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['list']['1']                                      = 'Mussar ils destinaturs da la funtauna da destinaturs cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['0']                              = 'Glistas da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['1']                              = 'Tscherna las glistas da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member']['0']                                    = 'Commembers da Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['0']           = 'Lubir da tscherner be ina gruppa';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['1']           = 'Lubir a l\'autur da tscherner be ina gruppa da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['0']     = 'Lubir da tscherner be ina glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['1']     = 'Lubir a l\'autur da tscherner be ina glista da mail da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['0']                = 'Lubir da tscherner be in commember';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['1']                = 'Lubir a l\'autur da tscherner be in commember da questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['0']                                  = 'Tscherner commembers&hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['1']                                  = 'Tscherna sco che commembers vegnan selecziunads.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllGroups']                              = 'tenor tut las gruppas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMailingLists']                        = 'tenor tut las glistas dad e-mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMembers']                             = 'tenor tut ils commembers';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroupMembers']                           = 'tenor ils commembers da las gruppas selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroups']                                 = 'tenor las gruppas tschernidas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingListMembers']                     = 'tenor ils commembers da las glistas dad e-mail selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingLists']                           = 'tenor las glistas dad e-mail selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['0']                     = 'Filter da colonnas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['1']                     = 'Filtrar ils destinatur tenor colonnas.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsComparator']['0']           = 'Cumparegliader';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsField']['0']                = 'Colonna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['0']             = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['1']             = 'Utilisar SQL navig (&rarr; la valur navegn betg controllà!).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsValue']['0']                = 'Valur';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['0']                              = 'Gruppas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['1']                              = 'Tscherna las gruppas selecziunadas.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['0']                        = 'Glistas da mail';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['1']                        = 'Tscherna las glistas da mail selecziunadas';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_details']                                 = 'Commembers da Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_legend']                                  = 'Commembers da Contao';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['missing_email_column']                           = 'Ti stos attribuir  <code>email</code> ad ina colonna!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['0']                                       = 'Nova funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['1']                                       = 'Crear ina nova funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['0']                                      = 'Detagls da la funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['1']                                      = 'Detagls da la funtauna da destinaturs cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['source_legend']                                  = 'Funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['0']                                     = 'Num';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['1']                                     = 'Endatescha in num per questa funtauna da destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['0']                                    = 'Activar/deactivar';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['1']                                    = 'Activar/deactivar la funtauna cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['0']                                      = 'Modul da funtauna per desintaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['1']                                      = 'Tscherna il modul che porscha las datas dals destinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['union']['0']                                     = 'Union';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['unionClean']['0']                                = 'Modus schuber';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['unionClean']['1']                                = 'En il modus schuber vegnan endatazins dublas filtradas. Quest modus dovra bler dapli cpu e memori e pudess esser plaun en per lungas glistas da recipients!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['unionRecipientSources']['0']                     = 'Funtauna da destinaturs';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['unionRecipientSources']['1']                     = 'Tscherna las funtaunas da destinaturs per unifitgar.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['union_legend']                                   = 'Union';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['0']                                        = 'Augmentar la prioritad';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['1']                                        = 'Augmentar la prioritad da la funtauna da destinaturs cun l\'ID %s';
