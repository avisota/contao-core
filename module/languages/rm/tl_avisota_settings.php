<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['0'] = 'Modul da transport da standard';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['1'] = 'Tscherna il modul da transport da standard.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['0']   = 'Adressa dad e-mail dal sviluppader';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['1']   = 'Endatescha l\'adressa dad e-mail che duai vegnir utilisada per mintga e-mail tramess en il modus da sviluppader';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['0']    = 'Modus da sviluppaders';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['1']    = 'Activar il modus da sviluppaders';
$GLOBALS['TL_LANG']['tl_avisota_settings']['developer_legend']               = 'Sviluppader';
$GLOBALS['TL_LANG']['tl_avisota_settings']['edit']                           = 'Configuraziun dal sistem dad Avisota';
$GLOBALS['TL_LANG']['tl_avisota_settings']['transport_legend']               = 'Transport';
