<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:09+01:00
 */

$GLOBALS['TL_LANG']['FMD']['avisota']                        = 'Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota-core']['0']              = 'Avisota - Core';
$GLOBALS['TL_LANG']['MOD']['avisota-core']['1']              = 'Integraziun da basa dad Avisota per Contao.';
$GLOBALS['TL_LANG']['MOD']['avisota']['0']                   = 'Newsletter Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_config']['0']            = 'Configuraziun';
$GLOBALS['TL_LANG']['MOD']['avisota_config:newsletter']['0'] = 'Configurar ils messadis';
$GLOBALS['TL_LANG']['MOD']['avisota_config:recipient']['0']  = 'Configuraziun dals destinaturs';
$GLOBALS['TL_LANG']['MOD']['avisota_config:transport']['0']  = 'Configuraziun da transport';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['0']      = 'Glistas da mail';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['1']      = 'Administrar las glistas da mail ch\'ins po abunar.';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter']['0']        = 'Messadis';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['0']  = 'Sboz da messadis';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['1']  = 'Administrar ils sboz per crear ordlunder novs newsletters.';
$GLOBALS['TL_LANG']['MOD']['avisota_outbox']['0']            = 'Posta sortida';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['0']             = 'Colonnas da spetga';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['1']             = 'Adminstrar las colonnas da spetga.';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['0']  = 'Funtaunas da destinaturs';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['1']  = 'Administrar las funtaunas da destinaturs.';
$GLOBALS['TL_LANG']['MOD']['avisota_recipients']['0']        = 'Destinaturs';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['0']        = 'Salids';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['1']        = 'Administrar ils salids.';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['0']          = 'Configuraziun da sistem dad Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['1']          = 'Administrar la configuraziun da basa dad Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_support']['0']           = 'Agid';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['0']             = 'Theme';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['1']             = 'Administrar ils themes, inclusivamain ils templates, stylesheets e la configuraziun dal layout da newsletters.';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['0']         = 'Transports';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['1']         = 'Administrar ils moduls da transportar.';
