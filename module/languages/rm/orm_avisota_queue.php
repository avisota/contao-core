<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:11+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['0']                    = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['1']                    = 'L\'alias da la colonna da spetga è ina referenza unica a la colonna da spetga che po vegnir inditgada empè da la ID.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['0']       = 'Lubir il trametter manual';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['1']       = 'Lubir als utilisaders dad exequir manualmain la colonna da spetga ed lur cuntegns';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clear']['0']                    = 'Svidar la colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clear']['1']                    = 'Svidar la colonna da spetga cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clearConfirm']                  = 'Vuls ti propi svidar la colonna da spetga?';
$GLOBALS['TL_LANG']['orm_avisota_queue']['config_legend']                 = 'Configuraziun da la colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['0']               = 'Pausa da ciclus';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['1']               = 'Endatescha il temp en secundas tranter mintga ciclus.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['0']                   = 'Stizzar la colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['1']                   = 'Stizzar la colonna da spetga cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['0']                     = 'Modifitgar la colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['1']                     = 'Modifitgar la colonna da spetga cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['0']             = 'Dumber/ciclus';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['1']             = 'Endatescha il dumber maximal dad e-mails tramess per ciclus.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['0']              = 'Temp da trametter';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['1']              = 'Endatescha il temp maximal en secundas per mintga ciclus.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['0']                      = 'Nova colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['1']                      = 'Crear ina nova colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['queueCleared']                  = 'La colonna da spetga <em>%s</em> è vegnida svidada.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['queue_legend']                  = 'Colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['0']         = 'Trametter a temp planisà';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['1']         = 'Utilisescha l\'algoritem per il trametter planisà per exequir a temp definì.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['send_legend']                   = 'Trametter';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['0']              = 'Grafica dals temps da trametter';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['1']              = 'Grafica dals temps da trametter che definescha ils temps d\'execuziun.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['0']                     = 'Detagls da la colonna da spetga';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['1']                     = 'Detagls da la colonna da spetga cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabase']                = 'Colonna da spetga simpla sin fundament d\'ina banca da datas';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['0'] = 'Num da tabella';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['1'] = 'Endatescha in num da tabella per la colonna da spetga. La tebella na duess <strong>betg</strong> cumenzar cun <em>tl_</em> u <em>orm_</em>!';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['0']                    = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['1']                    = 'Endatescha il titel da la colonna da spetga.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['0']                = 'Modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['1']                = 'Tscherna il modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport_legend']              = 'Configuraziun da transport';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['0']                     = 'Tip';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['1']                     = 'Tscherna il tip da la colonna da spetga';
