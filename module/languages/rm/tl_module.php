<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['tl_module']['avisota_categories']['0']                    = 'Categorias';
$GLOBALS['TL_LANG']['tl_module']['avisota_categories']['1']                    = 'Tscherna las categorias, ord las qualas ils Mailings duain vegnir mussads.';
$GLOBALS['TL_LANG']['tl_module']['avisota_cleanup_legend']                     = 'Far urden';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['0']                   = 'Pagina da destinaziun dal formular (na betg la pagina da confermaziun!)';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['1']                   = 'Tscherna ina pagina a la quala las datas da formular vegnan tramessas.';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_legend']                        = 'Glista dals mailings';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_template']['0']                 = 'Template da glista';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_template']['1']                 = 'Tscherna il template per la glista da mailings';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['0']                         = 'Glistas da mail';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['1']                         = 'Tscherna las glistas dad e-mail che vegnan abunadas u mussadas.';
$GLOBALS['TL_LANG']['tl_module']['avisota_mail_legend']                        = 'Configuraziun dad e-mail';
$GLOBALS['TL_LANG']['tl_module']['avisota_notification_legend']                = 'Regurdanza';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_legend']                      = 'Lectur dal mailing';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_template']['0']               = 'Template da lectur';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_template']['1']               = 'Tscherna il template per il lectur da mailings.';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['0']              = 'Datas persunalas';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['1']              = 'Tscherna las datas persunalas supplementaras che duain vegnir dumandadas.';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['0']                    = 'Mussar las glistas da mail';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['1']                    = 'Mussar las glistas da mail e far pussibel per ils destinaturs da tscherner tge glista da mail ch\'el vul abunar.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['0']   = 'Pagina da confermaziun da l\'abunament';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['1']   = 'Tscherna la pagina che vegn mussada suenter l\'abunar cun success.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscription_legend']                = 'Abunament';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['0']            = 'Template dal forumlar';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['1']            = 'Tscherna in template da forumlar persunalisà.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['0']         = 'Template dal formular';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['1']         = 'Tscherna in template persunalisà per il formular.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_unsubscribe']['0']          = 'Template per il forumular';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_unsubscribe']['1']          = 'Tscherna il template per il formular per de-abunar.';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['0'] = 'Pagina da confermaziun per de-abunar';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['1'] = 'Tscherna la pagina che vegn mussada suenter de-abunar cun success.';
$GLOBALS['TL_LANG']['tl_module']['avisota_view_page']['0']                     = 'Pagina da visualisaziun';
$GLOBALS['TL_LANG']['tl_module']['avisota_view_page']['1']                     = 'Tscherna qua ina pagina, sin la quala ils mailings duain vegnir visualisads. Na vegn nagina pagina tsschernida, vegn la pagina nudada en la categoria utilisada per la visualisaziun online';
