<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['1']              = 'L\'alias dal modul da transport è ina referenza unica al modul da transport che po vegnir inditgada empè da la ID.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['contact_legend']          = 'Configuraziun dad adressad da speditur e respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['0']             = 'Stizzar il modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['1']             = 'Stizzar il modul da transport cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['0']               = 'Modifitgar il modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['1']               = 'Modifitgar il modul da transport cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['0']        = 'Adressa dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['1']        = 'Endatescha l\'adressa dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['0']           = 'Num dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['1']           = 'Endatescha il num dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['0']                = 'Nov modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['1']                = 'Crear in nov modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['0']     = 'Adressa per respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['1']     = 'Endatescha l\'adressa per respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['0']        = 'Num per respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['1']        = 'Endatescha il num per respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['0']      = 'Adressa dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['1']      = 'Endatescha l\'adressa dal speditur.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['0']         = 'Num dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['1']         = 'Endatescha il num dal speditur.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service']                 = 'Servetsch persunalisà';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['0']        = 'Num dal servetsch';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['1']        = 'Endatescha il num dal servetsch';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service_legend']          = 'Servetsch';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['0']         = 'Definir l\'adressa per respostas';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['1']         = 'Definir l\'adressa per respostas per quest newsletter.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['0']          = 'Definir l\'adressa dal speditur';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['1']          = 'Definir l\'adressa dal speditur per quest newsletter.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['0']               = 'Detagls dal modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['1']               = 'Mussar ils detagls dal modul da transport cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift']                   = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['0']       = 'Criptadi da SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['1']       = 'Tscherna ina metoda da criptadi (SSL u TLS).';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['0']      = 'Num dal host SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['1']      = 'Endatescha il num dal host dal server da SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOff']            = 'Trametter ils e-mails per PHP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOn']             = 'Trametter ils e-mails per SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['0']      = 'Pled-clav SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['1']      = 'Endatescha il pled-clav per il server SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['0']      = 'Number dal port SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['1']      = 'Endatescha il number dal port dal server SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpSystemSettings'] = 'Utilisar la configuraziun dal sistem';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['0']      = 'Num d\'utilisader SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['1']      = 'Endatescha il num d\'utilisader per il server SMTP.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['0']       = 'Trametter ils e-mails per SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['1']       = 'Utilisar in server sa SMTP empà da la funcziun mail() da PHP per trametter e-mails.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift_legend']            = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['0']              = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['1']              = 'Tscherna il titel dal modul da transport.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['transport_legend']        = 'Modul da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['0']               = 'Tip da transport';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['1']               = 'Tscherna il tip da transport.';
