<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['avisota_outbox']['action_execute']        = 'Exequir ussa';
$GLOBALS['TL_LANG']['avisota_outbox']['col_duration']          = 'durada';
$GLOBALS['TL_LANG']['avisota_outbox']['col_failed']            = 'errur';
$GLOBALS['TL_LANG']['avisota_outbox']['col_length']            = 'Lunghezza';
$GLOBALS['TL_LANG']['avisota_outbox']['col_name']              = 'Num da la colonna da spetga';
$GLOBALS['TL_LANG']['avisota_outbox']['col_open']              = 'avrir';
$GLOBALS['TL_LANG']['avisota_outbox']['col_success']           = 'success';
$GLOBALS['TL_LANG']['avisota_outbox']['col_timeout']           = 'surpassà il temp';
$GLOBALS['TL_LANG']['avisota_outbox']['execute']               = 'Exequir la colonna da spetga %s';
$GLOBALS['TL_LANG']['avisota_outbox']['headline']              = 'Posta sortida';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_error']        = 'Ina errur è capitada, fermà l\'exequziun!';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_finish']       = 'Terminà da trametter, ti pos serrar la fanestra.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_initializing'] = 'Inizialisar, spetga e na serra betg la fanestra.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_pause']        = 'Spetgar sin il proxim ciclus, na betg serrar la fanestra';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_running']      = 'Trametter messadis, spetga e n a serra betg la fanestra';
