<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['0']                                     = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['1']                                     = 'L\'alias da la glista da mail è ina referenza unica a la glista da mail che po vegnir usilisada empè da l\'ID.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['0']                                      = 'Duplitgar la glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['1']                                      = 'Duplitgar la glista da mail cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['0']                                    = 'Stizzar la glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['1']                                    = 'Stizzar la gista da mail cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['0']                                      = 'Modifitgar la glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['1']                                      = 'Modifitgar la gista da mail cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['expert_legend']                                  = 'Configuraziun avanzada';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['0'] = 'Pagina per administrar ils abunaments da questa glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['1'] = 'Tscherna la pagina nua ch\'in abunent po administrar ils abunament per questa glista da mail.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_members']                                  = '%1$d commembers (<span title="%2$d commembers activs">%2$d</span> / <span title="%3$d commembers inactivs">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_recipients']                               = '%1$d abunents (<span title="%2$d abunents activs">%2$d</span> / <span title="%3$d abunents inactivs">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['list_legend']                                    = 'Glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['0']                                       = 'Nova glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['1']                                       = 'Agiuntar ina nova glista';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['0']                                      = 'Detagls da la glista da mail';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['1']                                      = 'Mussar ils detagls da la glista da mail cun l\'ID %s';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['0']                                     = 'Num';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['1']                                     = 'Endatscha il num da la glista da mail.';
