<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:25:53+01:00
 */

$GLOBALS['TL_LANG']['avisota_promotion']['donate'] = '<img src="assets/avisota/core/images/donate.svg" alt="">Avisota è in project dad Open Source.<br><a href="http://avisota.org" target="_blank">Donescha</a> e procura che il project haja in futur!';
