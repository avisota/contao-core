<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['0']              = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['1']              = 'Der Alias ist eine eindeutige Referenz zum Transportmodul und kann anstelle der ID benutzt werden.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['contact_legend']          = 'Details für Absende- und Antwortadressen';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['0']             = 'Transportmodul löschen';
$GLOBALS['TL_LANG']['orm_avisota_transport']['delete']['1']             = 'Löschen Sie das Transportmodul ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['0']               = 'Bearbeiten Sie Transportmodule.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['edit']['1']               = 'Bearbeiten Sie das Transportmodul ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['0']        = 'Absendeadresse';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromAddress']['1']        = 'Bitte geben Sie die Absendeadresse ein,';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['0']           = 'Absendername';
$GLOBALS['TL_LANG']['orm_avisota_transport']['fromName']['1']           = 'Bitte geben Sie den Namen des Absenders ein.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['0']                = 'Neues Transportmodul';
$GLOBALS['TL_LANG']['orm_avisota_transport']['new']['1']                = 'Erstellen Sie ein neues Transportmodul.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['0']     = 'Anwortadresse';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToAddress']['1']     = 'Bitte geben Sie die Antwortadresse an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['0']        = 'Antwortname';
$GLOBALS['TL_LANG']['orm_avisota_transport']['replyToName']['1']        = 'Bitte geben Sie den Antwortnamen ein.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['0']      = 'Senderadresse';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderAddress']['1']      = 'Bitte geben Sie die Absendeadresse an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['0']         = 'Sendername';
$GLOBALS['TL_LANG']['orm_avisota_transport']['senderName']['1']         = 'Bitte geben Sie den Absendernamen an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service']                 = 'Eigener Dienst';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['0']        = 'Dienstname';
$GLOBALS['TL_LANG']['orm_avisota_transport']['serviceName']['1']        = 'Bitte geben Sie den Dienstenamen an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service_legend']          = 'Service';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['0']         = 'Antwortadresse einstellen';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setReplyTo']['1']         = 'Legen Sie die Antwortadresse fest (als reply-to).';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['0']          = 'Absenderadresse festlegen';
$GLOBALS['TL_LANG']['orm_avisota_transport']['setSender']['1']          = 'Legen Sie die Absenderadresse für diesen Newsletter fest.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['0']               = 'Transportmodul-Details';
$GLOBALS['TL_LANG']['orm_avisota_transport']['show']['1']               = 'Zeigen Sie die Details des Transportmoduls ID %s an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift']                   = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['0']       = 'SMTP-Verschlüsselung';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['1']       = 'Hier können Sie die Verschlüsselungsmethode (SSL oder TLS) auswählen.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['0']      = 'SMTP-Hostname';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['1']      = 'Bitte geben Sie den Hostnamen des SMTP-Servers an.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOff']            = 'Mails per PHP-Funktion versenden';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpOn']             = 'Mails per SMTP versenden';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['0']      = 'SMTP-Passwort';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['1']      = 'Hier können Sie das SMTP-Passwort angeben.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['0']      = 'SMTP-Portnummer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['1']      = 'Bitte geben Sie die SMTP-Portnummer ein.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpSystemSettings'] = 'Systemeinstellungen benutzen';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['0']      = 'SMTP-Benutzername';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['1']      = 'Hier können Sie den SMTP-Benutzernamen angeben.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['0']       = 'Mails per SMTP senden';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftUseSmtp']['1']       = 'Benutzen Sie einen SMTP-Server anstelle der PHP mail()-Funktion, um die E-Mails zu senden.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swift_legend']            = 'Swift PHP Mailer';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['0']              = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['1']              = 'Bitte geben Sie den Titel des Transportmoduls ein.';
$GLOBALS['TL_LANG']['orm_avisota_transport']['transport_legend']        = 'Transportmodul';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['0']               = 'Transporttyp';
$GLOBALS['TL_LANG']['orm_avisota_transport']['type']['1']               = 'Bitte wählen Sie den Transporttyp aus.';
