<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */


$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['0']                                     = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['alias']['1']                                     = 'Der Alias ist eine eindeutige Referenz zur Mailingliste und kann anstelle der ID benutzt werden.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['0']                                      = 'Mailingliste duplizieren';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['copy']['1']                                      = 'Duplizieren Sie die Mailingliste ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['0']                                    = 'Mailingliste löschen';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['delete']['1']                                    = 'Löschen Sie die Mailingliste ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['0']                                      = 'Mailingliste bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['edit']['1']                                      = 'Bearbeiten Sie die Mailingliste ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['expert_legend']                                  = 'Experteneinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['0'] = 'Seite für Abonnementsverwaltung.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['integratedRecipientManageSubscriptionPage']['1'] = 'Wählen Sie die Seite aus, auf Empfänger ihr Abonnement für diese Mailingliste verwalten können.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_members']                                  = '%1$d Mitglieder (<span title="%2$d aktive Mitglieder">%2$d</span> / <span title="%3$d inaktive Mitglieder">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['label_recipients']                               = '%1$d Empfänger (<span title="%2$d aktive Empfänger">%2$d</span> / <span title="%3$d inaktive Empfänger">%3$d</span>)';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['list_legend']                                    = 'Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['0']                                       = 'Neue Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['new']['1']                                       = 'Fügen Sie eine neue Mailingliste hinzu.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['0']                                      = 'Mailinglisten-Details';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['show']['1']                                      = 'Zeigen Sie die Details der Mailingliste ID %s an.';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['0']                                     = 'Name';
$GLOBALS['TL_LANG']['orm_avisota_mailing_list']['title']['1']                                     = 'Bitte geben Sie den Namen der Mailingliste ein.';
