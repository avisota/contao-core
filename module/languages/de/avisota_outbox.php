<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:06+01:00
 */

$GLOBALS['TL_LANG']['avisota_outbox']['action_execute']        = 'Warteschlange jetzt abarbeiten';
$GLOBALS['TL_LANG']['avisota_outbox']['col_duration']          = 'Dauer';
$GLOBALS['TL_LANG']['avisota_outbox']['col_failed']            = 'fehlgeschlagen';
$GLOBALS['TL_LANG']['avisota_outbox']['col_length']            = 'Länge';
$GLOBALS['TL_LANG']['avisota_outbox']['col_name']              = 'Name der Warteschlange';
$GLOBALS['TL_LANG']['avisota_outbox']['col_open']              = 'öffen';
$GLOBALS['TL_LANG']['avisota_outbox']['col_success']           = 'erfolgreich';
$GLOBALS['TL_LANG']['avisota_outbox']['col_timeout']           = 'Wartezeit';
$GLOBALS['TL_LANG']['avisota_outbox']['execute']               = 'Warteschlange %s abarbeiten';
$GLOBALS['TL_LANG']['avisota_outbox']['headline']              = 'Postausgang';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_error']        = 'Ein Fehler ist aufgetreten. Die Ausführung wurde unterbrochen.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_finish']       = 'Versand vollständig. Sie können dieses Fenster jetzt schließen.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_initializing'] = 'Initialisierung. Bitte schließen Sie dieses Fenster nicht vor dem Abschluss.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_pause']        = 'Warten auf den nächsten Sendezyklus. Bitte schließen Sie dieses Fenster nicht vor dem Abschluss.';
$GLOBALS['TL_LANG']['avisota_outbox']['progress_running']      = 'Sende Nachrichten. Bitte lassen Sie dieses Fenster geöffnet und warten, bis der Versand abgeschlossen ist.';
