<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */

$GLOBALS['TL_LANG']['tl_module']['avisota_categories']['0']                    = 'Kategorien';
$GLOBALS['TL_LANG']['tl_module']['avisota_categories']['1']                    = 'Wählen Sie die Kategorien aus, aus denen die Mailing angezeigt werden sollen.';
$GLOBALS['TL_LANG']['tl_module']['avisota_cleanup_legend']                     = 'Aufräumen';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['0']                   = 'Zielseite (NICHT die Bestätigungssette)';
$GLOBALS['TL_LANG']['tl_module']['avisota_form_target']['1']                   = 'Bitte wählen Sie die Seite aus, auf der die übermittelten Formulardaten angezeigt werden.';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_legend']                        = 'Mailing-Liste';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_template']['0']                 = 'Listen-Template';
$GLOBALS['TL_LANG']['tl_module']['avisota_list_template']['1']                 = 'Wählen Sie hier das Template für die Mailing-Liste aus.';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['0']                         = 'Mailinglisten';
$GLOBALS['TL_LANG']['tl_module']['avisota_lists']['1']                         = 'Bitte wählen Sie die Mailinglisten aus, die abonniert oder angezeigt werden sollen.';
$GLOBALS['TL_LANG']['tl_module']['avisota_mail_legend']                        = 'Mail Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['avisota_notification_legend']                = 'Erinnerung';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_legend']                      = 'Mailing-Leser';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_template']['0']               = 'Leser-Template';
$GLOBALS['TL_LANG']['tl_module']['avisota_reader_template']['1']               = 'Wählen Sie hier das Template für den Mailing-Leser aus.';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['0']              = 'Persönliche Daten';
$GLOBALS['TL_LANG']['tl_module']['avisota_recipient_fields']['1']              = 'Bitte wählen Sie aus, welche Felder mit persönlichen Daten abgefragt werden sollen.';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['0']                    = 'Mailinglisten anzeigen';
$GLOBALS['TL_LANG']['tl_module']['avisota_show_lists']['1']                    = 'Lassen Sie die Mailinglisten anzeigen und ermöglichen ein Abonnement.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['0']   = 'Bestätigungsseite';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscribe_confirmation_page']['1']   = 'Bitte wählen Sie die Seite aus, die nach erfolgreicher Bestellung angezeigt wird.';
$GLOBALS['TL_LANG']['tl_module']['avisota_subscription_legend']                = 'Abonnement';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['0']            = 'Formular-Template';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscribe']['1']            = 'Hier können Sie ein individuelles Formular-Template auswählen.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['0']         = 'Formular-Template';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_subscription']['1']         = 'Bitte wählen Sie ein Formulartemplate aus.';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_unsubscribe']['0']          = 'Form-Template';
$GLOBALS['TL_LANG']['tl_module']['avisota_template_unsubscribe']['1']          = 'Wählen Sie hier das Template für das Abmelden-Formular aus.';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['0'] = 'Abmeldeseite';
$GLOBALS['TL_LANG']['tl_module']['avisota_unsubscribe_confirmation_page']['1'] = 'Bitte wählen Sie die Seite aus, die nach erfolgreicher Abbestellung angezeigt wird.';
$GLOBALS['TL_LANG']['tl_module']['avisota_view_page']['0']                     = 'Ansichtsseite';
$GLOBALS['TL_LANG']['tl_module']['avisota_view_page']['1']                     = 'Wählen Sie hier eine Seite aus, auf der die Mailing angezeigt werden soll. Wird keine Seite ausgewählt, wird die in der Kategorie hinterlegte Seite zur Online-Ansicht verwendet.';
