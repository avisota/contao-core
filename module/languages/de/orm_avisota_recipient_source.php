<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:12+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['0']                       = 'Spalten zuordnen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignment']['1']                       = 'Bitte ordnen Sie die Spalten den internen Feldern zu.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentColumn']['0']                 = 'Spalte';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['0']                  = 'Feld';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['1']                  = 'Bitte geben Sie den zuzuordnenden Feldnamen an. Beispiel: <code>forename</code> oder <code>surname</code>. Es muss mindestens die Spalte <code>email</code> zugeordnet werden.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiter']['0']                          = 'Feldbegrenzer';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiter']['1']                          = 'Bitte wählen Sie aus, welches Zeichen die Felder in der CSV-Datei begrenzt.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['comma']                     = 'Komma';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['linebreak']                 = 'Zeilenumbruch';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['separator']                 = 'Trennzeichen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['space']                     = 'Leerzeichen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileDelimiters']['tabulator']                 = 'Tabulator';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosures']['double']                    = 'Doppeltes Hochkomma';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileEnclosures']['single']                    = 'Hochkomma';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['0']                                = 'CSV-Datei';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['1']                                = 'Bitte wählen Sie die CSV-Datei aus.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFile_legend']                                 = 'CSV-Datei';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csv_file']['0']                                  = 'CSV-Datei';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['0']                                    = 'Empfängerquelle löschen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['delete']['1']                                    = 'Löschen Sie die Empfängerquelle ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['details_legend']                                 = 'Detaileinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['0']                                   = 'Deaktiviert';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['1']                                   = 'Deaktivieren Sie temporär diese Empfänger-Quelle für den Versand.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['0']                                      = 'Priorität vermindern';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['down']['1']                                      = 'Vermindern Sie die Priorität der Empfängerquelle ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['0']                                     = 'Zufallsgenerator (für Testzwecke)';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy']['1']                                     = 'Zufällig generierte Empfänger aus einer vorgegebenen Menge von Vornamen, Familiennamen und Domänen. Diese Empfänger Quelle eignet sich zum Testen. Bitte beachten Sie, dass alle E-Mails nur generiert werden. Diese sollten nicht existieren, aber es gibt keine Garantie! Diese Empfänger Quelle sollte nur in Kombination mit einem Dummy-Transport oder mit dem Entwickler-Modus verwendet.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['0']                             = 'Höchstanzahl';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMaxCount']['1']                             = 'Bitte geben Sie an, wie viele Empfänger höchstens vorhanden sein dürfen.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['0']                             = 'Mindestanzahl';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummyMinCount']['1']                             = 'Bitte geben Sie an, wie viele Empfänger mindestens vorhanden sein müssen.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['dummy_legend']                                   = 'Generatoreinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['duplicated_column']                              = 'Spalten und Felder können nicht mehrfach verwendet werden!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['0']                                      = 'Empfängerquelle bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['edit']['1']                                      = 'Bearbeiten Sie den Empfängerquelle ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['expert_legend']                                  = 'Experteneinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['0']                                    = 'Filter aktivieren';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['1']                                    = 'Aktivieren Sie Filter für diese Empfänger-Quelle. Die verfügbaren Filter hängen vom jeweiligen Modul ab.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filterByMailingLists']['0']                      = 'Nach Abonnement filtern';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filterByMailingLists']['1']                      = 'Filter nach Abonnements aktivieren';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter_legend']                                  = 'Filtereinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated']['0']                                = 'In Avisota integrierte Empfänger';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleListSelection']['0']        = 'Einzelauswahl erlauben';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedAllowSingleSelection']['0']            = 'Einzelauswahl erlauben';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['0']                              = 'Empfänger auswählen &hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedBy']['1']                              = 'Bitte wählen Sie, wie Empfänger ausgewählt werden.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllMailingLists']                    = 'nach allen Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByAllRecipients']                      = 'nach allen Empfängern';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByMailingLists']                       = 'nach ausgewählten Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedByRecipients']                         = 'nach Empfängern von ausgewählten Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['0']                         = 'Details zusammenführen aus &hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedDetails']['1']                         = 'Bitte wählen Sie aus, von woher die Details zusammengeführt werden sollen.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['0']                 = 'Spaltenfilter';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumns']['1']                 = 'Filtern Sie die Empfänger nach Spalten.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsComparator']['0']       = 'Vergleichsoperator';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsField']['0']            = 'Spalte';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['0']         = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsNoEscape']['1']         = 'Benutzt den Wert als natives SQL (&rarr; der Wert wird nicht escaped).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedFilterByColumnsValue']['0']            = 'Wert';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['0']          = 'Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedMailingListsRecipients']['1']          = 'Bitte wählen Sie die Mailingliste aus. Nur Empfänger, die diese Mailingliste abonniert haben sind verfügbar.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['0'] = 'Verwaltungsseite';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integratedRecipientManageSubscriptionPage']['1'] = 'Bitte wählen Sie die Seite aus, auf der das Abonnement verwalten wird.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_by_mailing_list']['0']                = 'In Avisota integrierte Empfänger ausgewählt aus der Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_details']                             = 'In Avisota integrierte Empfänger';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_legend']                              = 'In Avisota integrierte Empfänger';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['integrated_member_details']                      = 'In Avisota integrierte Empfänger und Contao-Mitglieder';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['list']['0']                                      = 'Empfänger auflisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['list']['1']                                      = 'Listen Sie die Empfänger aus der Quelle ID %s auf.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['0']                              = 'Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['1']                              = 'Bitte wählen Sie eine oder mehrere Mailinglisten aus.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member']['0']                                    = 'Contao-Mitglieder';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['0']           = 'Einzelauswahl von Gruppen erlauben';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleGroupSelection']['1']           = 'Gewähren sie dem Verfasser eine Einzelauswahl der Gruppen aus den Empfänger Quellen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['0']     = 'Einzelauswahl  von Mailinglisten erlauben';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleMailingListSelection']['1']     = 'Lassen Sie den Verfasser aus den Mailing-Listen einzelne Empfänger auswählen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['0']                = 'Einzelauswahl von Mitgliedern erlauben';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberAllowSingleSelection']['1']                = 'Gewähren sie dem Verfasser die Einzelauswahl der Mitglieder aus der Empfänger Quelle';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['0']                                  = 'Mitglieder auswählen &hellip;';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberBy']['1']                                  = 'Bitte wählen Sie die Methode zur Mitgliederauswahl.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllGroups']                              = 'nach allen Gruppen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMailingLists']                        = 'nach allen Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByAllMembers']                             = 'nach allen Mitgliedern';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroupMembers']                           = 'nach Mitgliedern aus gewählten Gruppen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByGroups']                                 = 'nach ausgewählten Gruppen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingListMembers']                     = 'nach Mitgliedern aus gewählten Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberByMailingLists']                           = 'nach ausgewählten Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['0']                     = 'Spaltenfilter';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['1']                     = 'Filtern Sie die Empfänger nach Spalten.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsComparator']['0']           = 'Vergleichsoperator';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsField']['0']                = 'Spalte';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['0']             = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['1']             = 'Benutzt den Wert als natives SQL (&rarr; der Wert wird nicht escaped).';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsValue']['0']                = 'Wert';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['0']                              = 'Gruppen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['1']                              = 'Bitte wählen Sie vorausgewählte Gruppen aus.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['0']                        = 'Mailinglisten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['1']                        = 'Bitte wählen Sie die ausgewählte Mailingliste';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_details']                                 = 'Contao-Mitglieder';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['member_legend']                                  = 'Contao-Mitglieder';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['missing_email_column']                           = 'Sie müssen <code>email</code> einer Spalte zuweisen!';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['0']                                       = 'Neuer Empfängerquelle';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['new']['1']                                       = 'Fügen Sie eine neue Empfängerquelle hinzu.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['0']                                      = 'Empfängerquellen-Details';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['show']['1']                                      = 'Zeigen Sie die Details der Empfängerquelle %s an.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['source_legend']                                  = 'Empfängerquelle';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['0']                                     = 'Name';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['1']                                     = 'Bitte geben Sie einen Namen für die Empfänger-Quelle an.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['0']                                    = 'Aktvierungsstatus umschalten';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['toggle']['1']                                    = 'Schaltet den Aktivierungsstatus der Empfänger-Quelle ID %s um.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['0']                                      = 'Empfängerquellen-Modul';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['type']['1']                                      = 'Bitte wählen Sie das Modul aus, das die Empfängerdaten zur Verfügung stellt.';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['0']                                        = 'Priorität erhöhen';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['up']['1']                                        = 'Erhöhen Sie die Priorität der Empfängerquelle ID %s.';

