<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T12:08:26+01:00
 */


$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['0'] = 'Standard-Transportmodul.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport']['1'] = 'Bitte wählen Sie das Standard-Transportmodul aus.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['0']   = 'Entwickler-Mailadresse.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['1']   = 'Bitte geben Sie die Mailadresse an, die im Entwicklermodus für jegliche Mail benutzt wird.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['0']    = 'Entwicklermodus';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['1']    = 'Aktivieren Sie den Entwicklermodus.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['developer_legend']               = 'Entwickler';
$GLOBALS['TL_LANG']['tl_avisota_settings']['edit']                           = 'Avisota-Systemeinstellungen';
$GLOBALS['TL_LANG']['tl_avisota_settings']['transport_legend']               = 'Transport';
