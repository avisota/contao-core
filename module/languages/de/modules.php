<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:08+01:00
 */

$GLOBALS['TL_LANG']['FMD']['avisota']                        = 'Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota-core']['0']              = 'Avisota - Core';
$GLOBALS['TL_LANG']['MOD']['avisota-core']['1']              = 'Basisintegration von Avisota für Contao.';
$GLOBALS['TL_LANG']['MOD']['avisota']['0']                   = 'Avisota-Newsletter';
$GLOBALS['TL_LANG']['MOD']['avisota_config']['0']            = 'Einstellungen';
$GLOBALS['TL_LANG']['MOD']['avisota_config:newsletter']['0'] = 'Nachrichteneinstellungen';
$GLOBALS['TL_LANG']['MOD']['avisota_config:recipient']['0']  = 'Empfängereinstellungen';
$GLOBALS['TL_LANG']['MOD']['avisota_config:transport']['0']  = 'Sendeeinstellungen';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['0']      = 'Mailinglisten';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['1']      = 'Abonierbare Mailinglisten verwalten';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter']['0']        = 'Nachrichten';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['0']  = 'Nachrichtenentwürfe';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter_draft']['1']  = 'Verwalten Sie Newslettervorlagen, aus denen neue Newsletter erstellt werden.';
$GLOBALS['TL_LANG']['MOD']['avisota_outbox']['0']            = 'Postausgang';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['0']             = 'Warteschlangen';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['1']             = 'Transport-Warteschlangen verwalten';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['0']  = 'Empfängerquellen';
$GLOBALS['TL_LANG']['MOD']['avisota_recipient_source']['1']  = 'Empfängerquellen verwalten';
$GLOBALS['TL_LANG']['MOD']['avisota_recipients']['0']        = 'Empfänger';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['0']        = 'Anreden';
$GLOBALS['TL_LANG']['MOD']['avisota_salutation']['1']        = 'Anreden bearbeiten';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['0']          = 'Avisota-Systemeinstellungen';
$GLOBALS['TL_LANG']['MOD']['avisota_settings']['1']          = 'Avisota-Systemeinstellungen bearbeiten';
$GLOBALS['TL_LANG']['MOD']['avisota_support']['0']           = 'Unterstützung';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['0']             = 'Themes';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['1']             = 'Verwalten Sie Themes, einschließlich der Templates,  Stylesheets und Layouteinstellungen für Newsletter.';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['0']         = 'Transport';
$GLOBALS['TL_LANG']['MOD']['avisota_transport']['1']         = 'Transportmodule verwalten';
