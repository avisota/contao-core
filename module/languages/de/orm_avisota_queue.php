<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:17:10+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['0']                    = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_queue']['alias']['1']                    = 'Der Alias einer Warteschlange ist eine eindeutige Referenz, die anstelle der ID benutzt werden kann.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['0']       = 'Manuellen Versand erlauben';
$GLOBALS['TL_LANG']['orm_avisota_queue']['allowManualSending']['1']       = 'Erlaubt Benutzern, den Versand aus der Warteschlange manuell anzustoßen und die Inhalte zu senden.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clear']['0']                    = 'Warteschlange leeren';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clear']['1']                    = 'Leeren Sie die Warteschlange ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['clearConfirm']                  = 'Möchten Sie die Warteschlange wirklich leeren?';
$GLOBALS['TL_LANG']['orm_avisota_queue']['config_legend']                 = 'Warteschlangen-Einstellungen';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['0']               = 'Zyklen-Pause';
$GLOBALS['TL_LANG']['orm_avisota_queue']['cyclePause']['1']               = 'Bitte geben Sie die Zeit in Sekunden an, die zwischen den Sendezyklen liegen soll.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['0']                   = 'Warteschlange löschen';
$GLOBALS['TL_LANG']['orm_avisota_queue']['delete']['1']                   = 'Löschen Sie die Warteschlange ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['0']                     = 'Warteschlange bearbeiten';
$GLOBALS['TL_LANG']['orm_avisota_queue']['edit']['1']                     = 'Bearbeiten Sie die Warteschlange ID %s.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['0']             = 'Anzahl der Sendungen';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendCount']['1']             = 'Bitte geben Sie die maximale Anzahl an Sendungen an, die für den Sendezyklus gilt.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['0']              = 'Sendedauer';
$GLOBALS['TL_LANG']['orm_avisota_queue']['maxSendTime']['1']              = 'Bitte geben Sie die Maximaldauer in Sekunden an, die dann für jeden Sendezyklus gilt.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['0']                      = 'Neue Warteschlange';
$GLOBALS['TL_LANG']['orm_avisota_queue']['new']['1']                      = 'Erstellen Sie eine neue Warteschlange.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['queueCleared']                  = 'Die Warteschlange <em>%s</em> wurde geleert.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['queue_legend']                  = 'Warteschlange';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['0']         = 'Versand planen';
$GLOBALS['TL_LANG']['orm_avisota_queue']['scheduledSending']['1']         = 'Den Algorithmus für automatische Ausführung des Versands benutzen.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['send_legend']                   = 'Versand';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['0']              = 'Sendeplan';
$GLOBALS['TL_LANG']['orm_avisota_queue']['sendingTime']['1']              = 'Sendeplan, der die Ausführungszeiten bestimmt.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['0']                     = 'Warteschlangen-Details';
$GLOBALS['TL_LANG']['orm_avisota_queue']['show']['1']                     = 'Zeigen Sie die Details der Warteschlange ID %s an.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabase']                = 'Einfache datenbankbasierte Warteschlange';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['0'] = 'Tabellenname';
$GLOBALS['TL_LANG']['orm_avisota_queue']['simpleDatabaseQueueTable']['1'] = 'Bitte geben Sie einen Tabellennamen für die Warteschlange an. Der Tabellenname sollte <strong>nicht</strong> mit<em>tl_</em> oder <em>orm_</em> beginnen.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['0']                    = 'Titel';
$GLOBALS['TL_LANG']['orm_avisota_queue']['title']['1']                    = 'Bitte geben Sie den Titel der Warteschlange an.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['0']                = 'Transportmodul';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport']['1']                = 'Bitte wählen Sie das Transportmodul aus.';
$GLOBALS['TL_LANG']['orm_avisota_queue']['transport_legend']              = 'Transporteinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['0']                     = 'Typ';
$GLOBALS['TL_LANG']['orm_avisota_queue']['type']['1']                     = 'Bitte wählen Sie den Typ der Warteschlange aus.';
