<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-11T15:25:53+01:00
 */

$GLOBALS['TL_LANG']['avisota_promotion']['donate'] = '<img src="assets/avisota/core/images/donate.svg" alt=""> Avisota ist ein Open Source-P rojekt.<br><a href="http://avisota.org" target="_blank">Spende</a> und unterstütze das Projekt!';
