<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:02:58+02:00
 */

$GLOBALS['TL_LANG']['avisota_promotion']['donate'] = '<img src="assets/avisota/core/images/donate.svg" alt=""> Avisota è un progetto Open Source<br><a href="http://avisota.org" target="_blank">Dona</a> e mantieni il progetto vivo!';
