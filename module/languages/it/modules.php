<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:03:00+02:00
 */

$GLOBALS['TL_LANG']['FMD']['avisota']                   = 'Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota']['0']              = 'Newsletter Avisota';
$GLOBALS['TL_LANG']['MOD']['avisota_config']['0']       = 'Impostazioni';
$GLOBALS['TL_LANG']['MOD']['avisota_mailing_list']['0'] = 'Mailing lists';
$GLOBALS['TL_LANG']['MOD']['avisota_newsletter']['0']   = 'Messaggi';
$GLOBALS['TL_LANG']['MOD']['avisota_queue']['0']        = 'Code';
$GLOBALS['TL_LANG']['MOD']['avisota_recipients']['0']   = 'Destinatari';
$GLOBALS['TL_LANG']['MOD']['avisota_support']['0']      = 'Supporto';
$GLOBALS['TL_LANG']['MOD']['avisota_theme']['0']        = 'Tema';
