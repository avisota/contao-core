<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:02:57+02:00
 */

$GLOBALS['TL_LANG']['avisota_outbox']['col_duration'] = 'durata';
$GLOBALS['TL_LANG']['avisota_outbox']['col_failed']   = 'fallito';
$GLOBALS['TL_LANG']['avisota_outbox']['col_open']     = 'aperto';
$GLOBALS['TL_LANG']['avisota_outbox']['col_success']  = 'successo';
