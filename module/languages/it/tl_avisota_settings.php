<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:03:07+02:00
 */

$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email']['0'] = 'Indirizzo email svilluppatore';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['0']  = 'Modalità svilluppatore';
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']['1']  = 'Abilità la modalità svilluppatore.';
$GLOBALS['TL_LANG']['tl_avisota_settings']['developer_legend']             = 'Svilluppatore';
$GLOBALS['TL_LANG']['tl_avisota_settings']['edit']                         = 'Impostazioni di sistema Avisota';
