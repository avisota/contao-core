<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:03:06+02:00
 */

$GLOBALS['TL_LANG']['orm_avisota_transport']['alias']['0']         = 'Alias';
$GLOBALS['TL_LANG']['orm_avisota_transport']['service_legend']     = 'Servizio';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpEnc']['0']  = 'Criptazione SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpHost']['0'] = 'Nome host SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPass']['0'] = 'Password SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpPort']['0'] = 'Numero porta SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['swiftSmtpUser']['0'] = 'Nome utente SMTP';
$GLOBALS['TL_LANG']['orm_avisota_transport']['title']['0']         = 'Titolo';
