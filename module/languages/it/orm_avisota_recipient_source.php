<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:03:04+02:00
 */

$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvColumnAssignmentField']['0']      = 'Campo';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFileSrc']['0']                    = 'File CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csvFile_legend']                     = 'File CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['csv_file']['0']                      = 'File CSV';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['disable']['0']                       = 'Disabilitato';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['filter']['0']                        = 'Abilita filtri';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['mailingLists']['0']                  = 'Mailing lists';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumns']['0']         = 'Filtro colonna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsField']['0']    = 'Colonna';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsNoEscape']['0'] = 'SQL';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberFilterByColumnsValue']['0']    = 'Valore';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberGroups']['0']                  = 'Gruppi';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['memberMailingLists']['0']            = 'Mailing lists';
$GLOBALS['TL_LANG']['orm_avisota_recipient_source']['title']['0']                         = 'Nome';
