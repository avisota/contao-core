<?php

$GLOBALS['TL_LANG']['avisota_promotion']['opensource']   =
    '<img src="assets/avisota/core/images/opensource.svg" alt=""> ' .
    'Avisota is an Open Source project.<br>' .
    '<a href="https://bitbucket.org/avisota" target="_blank">' .
    'View and validate the source online!' .
    '</a>';
$GLOBALS['TL_LANG']['avisota_promotion']['partners'] =
    '<img src="assets/avisota/core/images/partners.svg" alt=""> ' .
    '<a href="http://avisota.org/de/netzwerk/partner" target="_blank">' .
    'Avisota is backed by many partners.' .
    '</a><br>' .
    '<a href="http://avisota.org/de/netzwerk/partner#partner-werden" target="_blank">' .
    'Fill out application and back the project!' .
    '</a>!';
