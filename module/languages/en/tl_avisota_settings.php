<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-core
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
// transport
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_default_transport'] = array(
    'Default transport module',
    'Please choose the default transport module.'
);
// developer
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_mode']  = array(
    'Developer mode',
    'Enable the developer mode.'
);
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_developer_email'] = array(
    'Developer email address',
    'Please enter the email address to use for every email in developer mode.'
);

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_avisota_settings']['edit']             = 'Avisota system settings';
$GLOBALS['TL_LANG']['tl_avisota_settings']['transport_legend'] = 'Transport';
$GLOBALS['TL_LANG']['tl_avisota_settings']['developer_legend'] = 'Developer';
